package code

import scala.collection.mutable.Set
import scala.xml._


/**
  * Encapsulates the logic for exchanging attribute values and keeps track
  * of the statistics related to the attribute swap. As a state-encapsulating
  * class, some of the properties are mutable by necessity.
  */
class AttributeReplacementService {

  var replacements: Map[String, Int] = Map()

  val allPaths:Set[String] = scala.collection.mutable.Set()

  var encodedPaths = 0

  val indexInterval = 7

  var currentIndex:Int = -indexInterval

  /**
    * Generates a string with calculated number for unique/encoded paths and current index
    *
    * @return colon-separated string showing values
    */
  override def toString():String =
    encodedPaths.toString() + ":" + allPaths.size.toString() + ":" + currentIndex.toString()


  /**
    * Returns attribute value that is either the original value, or if the passed
    * value satisfies condition for swapping, a numeric ID to replace attribute value
    * with. Updates the ID-to-attribute-value map and increments counters when necessary
    *
    * @param node The sequence of attributes for a particular node
    * @param attr The attribute to analyze and return a value for
    *
    * @return The updated attribute value
    */
  def getAttributeValue(node: MetaData, attr: String): String = {
    val attributeValue = node.get(attr).getOrElse("").toString()

    // Add the path to our set if we haven't seen the URL yet. Allows
    // to determine the unique number of URLs we've seen in the document
    if (!allPaths.contains(attributeValue)) {
      allPaths += attributeValue
    }

    // Keep track of all the URLs we've seen
    encodedPaths += 1

    // Swap the attribute value with the expected result, either the original
    // value if doesn't match condition, or a numeric value if it does.
    if (attributeValue.containsSlice("cust") && !attributeValue.containsSlice("internal")) {
      replacements.get(attributeValue) match {
        case Some(index) => {
          index.toString()
        }
        case None => {
          currentIndex += indexInterval
          replacements += (attributeValue -> currentIndex)
          currentIndex.toString()
        }
      }
    }
    else attributeValue
  }


  /**
    * Translates the String -> Int map into a Int -> String map to get a mapping
    * of IDs to URLs instead of URLs to IDs
    *
    * @return
    */
  def invertMap(): Map[Int, String] =
    replacements map {case (key, value) => (value, key)}


  /**
    * Generates expected case class, populated with the values from the
    * attribute swap
    *
    * @param documentNode The document node of the modifued XML/HTML
    *
    * @return
    */
  def getSwapStatistics(documentNode: Node): PathTrackerResult =
    PathTrackerResult(documentNode.toString(), invertMap(), allPaths.size, encodedPaths)

}
