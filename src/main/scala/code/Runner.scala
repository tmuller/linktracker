package code

import scala.xml._

object Runner {
  def main(args: Array[String]): Unit = {

    val document =
      <html>
        <head>
          <title>Some Doc</title>
          <link rel="stylesheet" href="/styles/mystyle.css"/>
          <link rel="stylesheet" href="/styles/custstyles/mystyle.css"/>
          <style type="text/css">
            {""".blah { background-image: url('/img/happycustomers.jpg'); }"""}
          </style>
        </head>
        <body>
          Some intro text about mypic and something.
          <a href="/something/about/customers">GO HERE</a>
          <img alt="test alt" src="/img/mypic.jpg"/>
          <img alt="another alt attribute" src="/img/happycustomers.jpg"/>
          <img alt="another alt attribute" src="/img/internalcustonly/mypic.jpg"/>
          <img alt="another alt attribute" src="/img/mypic.jpg"/>
          <a class="oink-men united" href="/something/about/customers">OR HERE</a>
        </body>
      </html>

    val result = LinkTracker.parseDoc(document)
    println(result.trackedDoc.toString())

  }


}
