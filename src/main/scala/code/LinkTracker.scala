package code

import scala.xml._

/**
 * Link Tracking
 *
 * Given an HTML document as Scala XML:
 *
 * For each a element with an href attribute or img element with a src attribute
 * in the document if the attribute value contains "cust" but not "internal"
 * replace it with a unique numeric ID.  The IDs should start at 0 and increment
 * by 7.  If the path matches one we've already seen reuse the old ID.
 *
 * Return the updated HTML document along with a Map of IDs to paths wrapped in
 * a PathTrackerResult.
 */


object LinkTracker {
  def parseDoc(doc: Elem): PathTrackerResult = {

    val urlService = new AttributeReplacementService()

    val result = traverseXml(urlService)(doc)
    urlService.getSwapStatistics(result)
  }


  /**
    * Recursively parses the HTML/XML tree and initiates the analysis and modification
    * of the XML.
    *
    * @param service  Class abstracting out logic to swap attribute values
    * @param xmlNode  TThe root node of the current tree
    * @return
    */
  def traverseXml(service: AttributeReplacementService)(xmlNode: Node): Node = xmlNode match {
    case node @ Elem(_, "img", attrs, _, ch @ _*) =>
      node.asInstanceOf[Elem] % Attribute("", "src", service.getAttributeValue(attrs, "src"), Null) copy(child = ch map traverseXml(service))
    case node @ Elem(_, "a", attrs, _, ch @ _*) =>
      node.asInstanceOf[Elem] % Attribute("", "href", service.getAttributeValue(attrs, "href"), Null) copy(child = ch map traverseXml(service))
    case node @ Elem(_, _, _, _, ch @ _*) =>
      node.asInstanceOf[Elem].copy(child = ch map traverseXml(service))
    case other => other
  }

}

case class PathTrackerResult (trackedDoc: String, encodedPaths: Map[Int, String],
                              uniquePaths: Int, totalPaths: Int)

